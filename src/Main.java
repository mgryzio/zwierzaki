import Potrawy.Potrawa;
import Potrawy.nazwaPotrawy;
import Zwierzeta.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        // przedstawianie się zwierząt

        Kon horse = new Kon();
        horse.setImie("Mustang z Zielonej Doliny");

        Kot cat = new Kot();
        cat.setImie("Mruczek");

        Golab pigeon = new Golab();
        pigeon.setImie("Orł");

        Pies dog = new Pies();
        dog.setImie("Kundel Bury");

        Zwierz[] stadko = new Zwierz[4];
        stadko[0] = cat;
        stadko[1] = horse;
        stadko[2] = dog;
        stadko[3] = pigeon;

        for (Zwierz x : stadko) {
            System.out.println(x);
        }

        // lista potraw

        ArrayList<Potrawa> ulubionePotrawyKota = new ArrayList<>();
        Potrawa ulubionaPotrawaKotaRybka = new Potrawa(nazwaPotrawy.ryba, 0.4f);
        ulubionePotrawyKota.add(ulubionaPotrawaKotaRybka);
        Potrawa ulubionaPotrawaKotaMleko = new Potrawa(nazwaPotrawy.mleko, 0.5f);
        ulubionePotrawyKota.add(ulubionaPotrawaKotaMleko);

        ArrayList<Potrawa> ulubionaPotrawaPsa = new ArrayList<>();
        Potrawa ulubionaPotrawaPsaBoczek = new Potrawa(nazwaPotrawy.boczek, 1.5f);
        ulubionaPotrawaPsa.add(ulubionaPotrawaPsaBoczek);
        Potrawa ulubionaPotrawaPsaWoda = new Potrawa(nazwaPotrawy.woda, 2);
        ulubionaPotrawaPsa.add(ulubionaPotrawaPsaWoda);

        ArrayList<Potrawa> ulubioneJedzenieKonia = new ArrayList<>();
        Potrawa jedzonkoKoniaSianko = new Potrawa(nazwaPotrawy.siano, 6);
        ulubioneJedzenieKonia.add(jedzonkoKoniaSianko);
        Potrawa jedzonkoKoniaTrawa = new Potrawa(nazwaPotrawy.trawa, 3.2f);
        ulubioneJedzenieKonia.add(jedzonkoKoniaTrawa);

        ArrayList<Potrawa> ulubionaPotrawaGolebia = new ArrayList<>();
        Potrawa ulubioneJedzenieGolebiaBulka = new Potrawa(nazwaPotrawy.bulka, 0.2f);
        ulubionaPotrawaGolebia.add(ulubioneJedzenieGolebiaBulka);
        Potrawa ulubioneJedzenieGolebiaZiarna = new Potrawa(nazwaPotrawy.ziarna, 0.3f);
        ulubionaPotrawaGolebia.add(ulubioneJedzenieGolebiaZiarna);


        System.out.println("");
        System.out.println("Dania gołębia: ");

        for (Potrawa dania : ulubionaPotrawaGolebia) {
            System.out.println(dania);
        }

        // przypisywanie ulubionych potraw do zwierząt

        horse.setUlubionaPotrawa(ulubioneJedzenieKonia);
        cat.setUlubionaPotrawa(ulubionePotrawyKota);
        dog.setUlubionaPotrawa(ulubionaPotrawaPsa);
        pigeon.setUlubionaPotrawa(ulubionaPotrawaGolebia);

    }
}
