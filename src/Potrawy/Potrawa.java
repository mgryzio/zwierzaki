package Potrawy;

public class Potrawa {

    protected nazwaPotrawy nazwa;
    protected float ilosc;

    public Potrawa(nazwaPotrawy nazwa, float ilosc) {
        this.nazwa = nazwa;
        this.ilosc = ilosc;
    }

    public nazwaPotrawy getNazwa() {
        return nazwa;
    }

    public void setNazwa(nazwaPotrawy nazwa) {
        this.nazwa = nazwa;
    }

    public float getIlosc() {
        return ilosc;
    }

    public void setIlosc(float ilosc) {
        this.ilosc = ilosc;
    }

    @Override
    public String toString() {
        return String.format("Ulubiona potrawa to %s w liczbie %s kg.", nazwa.getNazwa(), ilosc);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) return false;

        Potrawa potrawa = (Potrawa) o;

        return nazwa == potrawa.nazwa;
    }

    @Override
    public int hashCode() {
        return nazwa.hashCode();
    }
}