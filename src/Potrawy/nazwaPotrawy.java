package Potrawy;

public enum nazwaPotrawy {

    mleko("mleko"),
    kosci("kości"),
    szynka("szynka"),
    chleb("chleb"),
    karmaDlaKota("karma dla kota"),
    karmaDlaPsa("karma dla psa"),
    woda("woda"),
    boczek("boczek"),
    ser_bialy("ser biały"),
    ryba("syba"),
    platki("płatki"),
    ogorek("ogórek"),
    ser_zolty("żółty ser"),
    bulka("bułka"),
    siano("siano"),
    trawa("trawa"),
    ziarna("ziarna"),
    pasza("pasza");

    public String nazwa;
    nazwaPotrawy(String nazwa){
        this.nazwa = nazwa;
    }
    public String getNazwa() {
        return nazwa;
    }
}

