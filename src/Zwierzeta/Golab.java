package Zwierzeta;

import Potrawy.Potrawa;

import java.util.ArrayList;

public class Golab extends Zwierz {

    private String glos = "Grrruuu, gruu...";
    protected ArrayList<Potrawa> ulubionaPotrawa;

    public ArrayList<Potrawa> getUlubionaPotrawa() {
        return ulubionaPotrawa;
    }

    public void setUlubionaPotrawa(ArrayList<Potrawa> ulubionaPotrawa) {
        this.ulubionaPotrawa = ulubionaPotrawa;
    }

    @Override
    public String dajGlos() {
        return String.format("%s", glos);
    }

    public Golab() {
        this("");
    }

    public Golab(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return String.format("Nazywam się %s! %s", getImie(), dajGlos());
    }
}
