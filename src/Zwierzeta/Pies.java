package Zwierzeta;

import Potrawy.Potrawa;

import java.util.ArrayList;

public class Pies extends Zwierz {

    private String glos = "Hau, hau, HAAAAUUUU!";
    protected ArrayList<Potrawa> ulubionaPotrawa;

    public ArrayList<Potrawa> getUlubionaPotrawa() {
        return ulubionaPotrawa;
    }

    public void setUlubionaPotrawa(ArrayList<Potrawa> ulubionaPotrawa) {
        this.ulubionaPotrawa = ulubionaPotrawa;
    }

    @Override
    public String dajGlos() {
        return String.format("%s", glos);
    }

    public Pies(){
        this("");
    }

    public Pies(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return String.format("Nazywam się %s! %s", getImie(), dajGlos());
    }

    public static void main(String[] args) {
        Pies k = new Pies();
        k.setImie("Burek");
        System.out.println(k.toString());

    }

}
