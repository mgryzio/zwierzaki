package Zwierzeta;

import Potrawy.Potrawa;

import java.util.ArrayList;

public class Kot extends Zwierz {

    private String glos = "Miau, miau...";
    protected ArrayList<Potrawa> ulubionaPotrawa;

    public ArrayList<Potrawa> getUlubionaPotrawa() {
        return ulubionaPotrawa;
    }

    public void setUlubionaPotrawa(ArrayList<Potrawa> ulubionaPotrawa) {
        this.ulubionaPotrawa = ulubionaPotrawa;
    }

    @Override
    public String dajGlos() {
        return String.format("%s", glos);
    }

    public Kot() {

    }

    public Kot(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return String.format("Nazywam się %s! %s", getImie(), dajGlos());
    }

    @Override
    public void setUlubionePotrawy(ArrayList<Potrawa> ulubionePotrawy) {
        super.setUlubionePotrawy(ulubionePotrawy);
    }
}
