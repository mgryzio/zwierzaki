package Zwierzeta;

import Potrawy.Potrawa;

import java.util.ArrayList;

public class Zwierz {

    protected String imie;
    protected String gatunek;
    protected ArrayList<Potrawa> ulubionePotrawy;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String dajGlos() {
        return String.format("");
    }

    public String toString() {
        return String.format("Moje imię to %s", imie);
    }

    public ArrayList<Potrawa> getUlubionePotrawy() {
        return ulubionePotrawy;
    }

    public void setUlubionePotrawy(ArrayList<Potrawa> ulubionePotrawy) {
        this.ulubionePotrawy = ulubionePotrawy;
    }

    // indexOf wykorzystuje metodę equals, przez co możemy sprawdzić na którym indexie znajduje się dodawana wartość, o ile w ogóle znajduje się na liście

    public void dodawanieUlubionejPotrawy(Potrawa jedzonko) {
        int x = getUlubionePotrawy().indexOf(jedzonko);

        if (x == -1) {
            getUlubionePotrawy().add(jedzonko);
        } else {
            Potrawa potrawa = getUlubionePotrawy().get(x);
            float ilosc1 = potrawa.getIlosc();
            float ilosc2 = jedzonko.getIlosc();
            potrawa.setIlosc(ilosc1 + ilosc2);
        }
    }


}
