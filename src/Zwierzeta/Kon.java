package Zwierzeta;

import Potrawy.Potrawa;

import java.util.ArrayList;

public class Kon extends Zwierz {

    private String glos = "Ihaaaaaaa!";
    protected ArrayList<Potrawa> ulubionaPotrawa;

    public ArrayList<Potrawa> getUlubionaPotrawa() {
        return ulubionaPotrawa;
    }

    public void setUlubionaPotrawa(ArrayList ulubionaPotrawa) {
        this.ulubionaPotrawa = ulubionaPotrawa;
    }

    @Override
    public String dajGlos() {
        return String.format("%s", glos);
    }

    public Kon(){}

    public Kon(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return String.format("Nazywam się %s! %s", getImie(), dajGlos());
    }
}